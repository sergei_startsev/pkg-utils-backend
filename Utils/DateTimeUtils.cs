﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBIT.Utils
{
    public class DateTimeUtils
    {
        public static DateTime DoubleToDateTime(double t)
        {
            DateTime dateTime = new DateTime(1899, 12, 30);
            dateTime = dateTime.AddDays((int)t);
            System.TimeSpan duration = new System.TimeSpan(0, 0, 0, (int)Math.Round(((t - Math.Floor(t)) * 86400), 0));
            dateTime = dateTime.Add(duration);
            return dateTime;
        }

        public static string DoubleToDateTimeToString(double t, string formatter)
        {
            if (t == 0)
            {
                return "";
            }
            DateTime dateTime = new DateTime(1899, 12, 30);
            dateTime = dateTime.AddDays((int)t);
            System.TimeSpan duration = new System.TimeSpan(0, 0, 0, (int)Math.Round(((t - Math.Floor(t)) * 86400), 0));
            dateTime = dateTime.Add(duration);
            return dateTime.ToString(formatter);
        }

        public static Double DateTimeToDouble(DateTime t)
        {
            DateTime dateTime = new DateTime(1899, 12, 30);
            TimeSpan result = t.Subtract(dateTime);
            int days = result.Days;
            int seconds = result.Hours * 60 * 60 + result.Minutes * 60 + result.Seconds;
            return days + seconds / 86400.0;
        }
    }
}
