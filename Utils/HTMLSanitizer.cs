﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace CBIT.Utils
{
    public static class HTMLSanitizer
    {
        private static readonly IDictionary<string, string> Symbols;

        private static string[] whiteTags;

        static HTMLSanitizer()
        {
            whiteTags= new[] {"a", "strong", "i", "em", "blockquote", "b", "p", "ul", "ol", "li", "div", "u",  "table", "tr", "td", "th", "br", "span"};

            Symbols = new Dictionary<string, string>{
                {"<", "&lt;"},
                {">", "&gt;"},
                {"\"","&quot;"},
                {"&","&amp"},
                {"\'","&#39;"}
            };
        }
        /// <summary>
        /// Sanitazes HTML fragment with default whiteTagList 
        /// </summary>
        /// <param name="htmlFragment">fragment to sanitize</param>
        public static string Sanitize(string htmlFragment)
        {
            return SanitizeHTML(htmlFragment, whiteTags);
        }
        /// <summary>
        /// Sanitazes HTML fragment with whiteTagList 
        /// </summary>
        /// <param name="htmlFragment">fragment to sanitize</param>
        /// <param name="whiteTagList">array of HTML tags without brackets</param>
        public static string Sanitize(string htmlFragment, string[] whiteTagList)
        {
            return SanitizeHTML(htmlFragment, whiteTagList);
        }

        private static string SanitizeHTML(string htmlFragment, string[] whiteTagList)
        {
            htmlFragment = HttpContext.Current.Server.HtmlEncode(htmlFragment);
            whiteTags = whiteTagList;
            foreach (string tag in whiteTags)
            {
                string patternString = @"(&lt;/?" + tag + @".*?&gt;)";
                string tmp;
                foreach (Match match in Regex.Matches(htmlFragment, patternString))
                {
                    tmp = match.Value;
                    foreach (KeyValuePair<string, string> symbol in Symbols)
                    {
                        tmp = Regex.Replace(tmp, symbol.Value, symbol.Key);
                    }
                    htmlFragment = Regex.Replace(htmlFragment, Regex.Escape(match.Value), tmp);
                }
            }
            return htmlFragment;
        }
    }
}