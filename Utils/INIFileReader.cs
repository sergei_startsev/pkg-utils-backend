﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBIT.Utils
{
    public class INIFileReader
    {
        public static Dictionary<string, string> ReadFile(string filePath)
        {
            var data = new Dictionary<string, string>();
            foreach (var row in File.ReadAllLines(filePath))
                data.Add(row.Split('=')[0].Trim(), string.Join("=", row.Split('=').Skip(1).ToArray()));

            return data;
        }
    }
}
