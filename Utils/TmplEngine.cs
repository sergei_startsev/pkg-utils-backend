﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Xml;
using Newtonsoft.Json;

namespace CBIT.Utils
{
    public static class TmplEngine
    {
        public const string RootDirectoryName = "dataRoot";
        public static readonly Dictionary<string, string> ReservedWords = new Dictionary<string, string>
                                                               {
                                                                   {"PrefixStart","{{"},
                                                                   {"PrefixEnd", "}}"},
                                                                   {"PrefixStartValue", "="},
                                                                   {"PrefixEndValue", ""},
                                                                   {"PrefixOpenStartLoop","each"},
                                                                   {"PrefixOpenEndLoop",""},
                                                                   {"PrefixCloseStartLoop","/each"},
                                                                   {"PrefixCloseEndLoop",""},
                                                                   {"PrefixVariable","$"},
                                                               };

        public static string GenerateDoc(string strTmpl, XmlDocument xmlData)
        {
            string outDoc = ScanTmplSax(strTmpl, xmlData.SelectSingleNode(RootDirectoryName));
            return outDoc;
        }

        public static string GenerateDoc(StreamReader fileTmpl, string jsonData)
        {
            return GenerateDoc(fileTmpl.ReadToEnd(), jsonData);
        }

        public static string GenerateDoc(string strTmpl, string jsonData)
        {
            return GenerateDoc(strTmpl, JsonConvert.DeserializeXmlNode(jsonData, RootDirectoryName));
        }

        private static string ScanTmplSax(string tmpl, XmlNode data, int indexCounter = 0)
        {
            // определяем позицию первого тега в шаблоне
            int startIndex = tmpl.IndexOf(ReservedWords["PrefixStart"], 0, StringComparison.InvariantCulture);
            if (startIndex == -1) // нет ни одного тега
                return tmpl;
            int endIndex = tmpl.IndexOf(ReservedWords["PrefixEnd"], startIndex, StringComparison.InvariantCulture) + ReservedWords["PrefixEnd"].Length;
            // смотрим это цикл? и вычленяем название тега
            string strTag = tmpl.Substring(startIndex, endIndex - startIndex);
            bool isLoop = IsLoop(strTag);
            string tagName = GetTagName(strTag);
            var result = new StringBuilder();
            result.Append(tmpl.Substring(0, startIndex));
            // если еденичный тег (не цикл) ищем данные по имени тега в xml и передаем их в результат
            if (!isLoop)
            {
                // индекс заменяем на значение счетчика
                if (tagName.Equals(ReservedWords["PrefixVariable"] + "index"))
                {
                    result.Append(indexCounter.ToString(CultureInfo.InvariantCulture));
                }
                else
                {
                    var singleNode = data.SelectSingleNode("./" + tagName);
                    result.Append(
                        (singleNode != null && singleNode.FirstChild != null && singleNode.FirstChild.Value != null) ?
                        singleNode.FirstChild.Value :
                        "");//"[нет данных]");
                }
            }
            // если цикл ищем закрывающий позицию закрывающего тега, 
            // смотрим в xml сколько итераций цикла нужно сделать,
            // вычленяем тело цикла и входим в рекурсию, в каждой итерации инкрементируем счетчик для индекса
            else
            {
                startIndex = endIndex;
                endIndex = tmpl.IndexOf(ReservedWords["PrefixStart"] + ReservedWords["PrefixCloseStartLoop"] +
                    " " + tagName + ReservedWords["PrefixCloseEndLoop"] + ReservedWords["PrefixEnd"],
                        endIndex, StringComparison.InvariantCulture);
                var listNode = data.SelectNodes("./" + tagName);
                if (listNode != null && listNode.Count > 0)
                {
                    int index = indexCounter;
                    foreach (XmlNode node in listNode)
                    {
                        index++;
                        result.Append(ScanTmplSax(tmpl.Substring(startIndex, endIndex - startIndex), node, index));
                    }
                }
                else
                {
                    result.Append(ScanTmplSax(tmpl.Substring(startIndex, endIndex - startIndex), data));
                }
                // выходим за тело цикла
                endIndex += ReservedWords["PrefixStart"].Length + ReservedWords["PrefixCloseStartLoop"].Length +
                    " ".Length + tagName.Length + ReservedWords["PrefixCloseEndLoop"].Length + ReservedWords["PrefixEnd"].Length;
            }
            // смотрим позицию последнего тега, если она больше позиции текущего => входим в рекурсию с оставшимся куском шаблона
            int count = tmpl.LastIndexOf(ReservedWords["PrefixEnd"], tmpl.Length, StringComparison.InvariantCulture); // rename variable
            if (endIndex <= count)
            {
                result.Append(ScanTmplSax(tmpl.Substring(endIndex), data, indexCounter));
                return result.ToString();
            }
            // приклееваем к результату оставшуюся часть шаблона (без тегов)
            result.Append(tmpl.Substring(endIndex));
            return result.ToString();
        }

        private static string GetTagName(string tag)
        {
            // любое слово с $ в начале или без него
            const string pattern = @"\$?\w+";
            MatchCollection matches = Regex.Matches(tag, pattern);
            // в теге только 1 парамметр, остальное невалидно
            foreach (Match match in matches)
            {
                if (!IsReserve(match.Value))
                    return match.Value;
            }
            return null;
        }

        private static bool IsLoop(string tag)
        {
            return tag.Contains(ReservedWords["PrefixOpenStartLoop"]);
        }

        private static bool IsReserve(string word)
        {
            return ReservedWords.ContainsValue(word);
        }
    }
}
